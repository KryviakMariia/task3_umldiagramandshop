package com.epam.task3;

import com.epam.task3.enums.Duster;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] arg) {
        Main main = new Main();

        main.chooseOperation();

    }

    private void printMenu() {
        System.out.println("You are in 'Household Cleaning Products' department");
        System.out.println("Please choose option:");
        System.out.println("1. Sort products");
        System.out.println("2. Search product by name");
        System.out.println("3. Exit");

    }

    private String searchProduct() {
        System.out.println("Please enter the name of product");
        Scanner sc = new Scanner(System.in);
        String findName = sc.next();
        System.out.println(Duster.getDusterByName(findName));

        return findName;
    }

    private void printSorted() {

        List<Duster> duster = Duster.getSortedDusters();

        for (Duster str : duster) {
            System.out.println(str);
        }
    }

    private void chooseOperation() {
        int operation;
        do {
            printMenu();
            Scanner scanner = new Scanner(System.in);
            operation = scanner.nextInt();
            switch (operation) {
                case 1:
                    printSorted();
                    break;
                case 2:
                    searchProduct();
                    break;
                case 3:
                    operation = 3;
                    break;
                default:
                    System.out.println("Invalid operation. Please choose from 1 to 3");
            }
        } while (operation != 3);
        System.exit(0);
    }
}
