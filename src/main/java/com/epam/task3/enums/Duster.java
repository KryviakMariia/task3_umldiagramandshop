package com.epam.task3.enums;

import java.util.*;

public enum Duster {

    SOAP("Soap", 10.50),
    POWDER("Washing powder", 25.00),
    SPONGE("Sponge", 20.25),
    RAG("Duster", 5.99),
    BROOM("Broom", 30.00),
    SWAB("Swab", 150.00),
    BUCKET("Bucket", 47.60),
    BIN("Bin", 39.99);


    private final String name;
    private final double price;

    Duster(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public static Duster getDusterByName(String name) {
        for (Duster duster: Duster.values()) {
            if (duster.name.equalsIgnoreCase(name)) {
                return duster;
            }
        }
        return null;
    }
    public static List<Duster> getSortedDusters() {
        List<Duster> dustersList = new ArrayList<>(EnumSet.allOf(Duster.class));
        Collections.sort(dustersList, (a, b) -> a.price < b.price ? -1 : a.price == b.price ? 0 : 1);
        return dustersList;
    }

    @Override
    public String toString() {
        return "Duster Item: [" + this.name + ", " + this.price + "]";
    }
}
